package ru.tsk.vkorenygin.tm.enumerated;

import ru.tsk.vkorenygin.tm.comparator.ComparatorByCreated;
import ru.tsk.vkorenygin.tm.comparator.ComparatorByName;
import ru.tsk.vkorenygin.tm.comparator.ComparatorByStartDate;
import ru.tsk.vkorenygin.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.INSTANCE),
    NAME("Sort by name", ComparatorByName.INSTANCE),
    START_DATE("Sort by date start", ComparatorByStartDate.INSTANCE),
    STATUS("Sort by status", ComparatorByStatus.INSTANCE);

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

    public Comparator getComparator() {
        return comparator;
    }

}
