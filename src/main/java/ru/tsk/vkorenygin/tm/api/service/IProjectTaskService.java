package ru.tsk.vkorenygin.tm.api.service;

import ru.tsk.vkorenygin.tm.model.Project;
import ru.tsk.vkorenygin.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    Task bindTaskToProject(final String projectId, final String taskId);

    Task unbindTaskFromProject(final String projectId, final String taskId);

    List<Task> findAllTasksByProjectId(final String id);

    Project removeProjectById(final String id);

    Project removeProjectByIndex(final int index);

    Project removeProjectByName(final String name);

}
